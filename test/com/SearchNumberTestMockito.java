package com;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by Евгений on 02.08.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchNumberTestMockito  {
    @Mock
    private  SearchNumber searchNumber;
    @Before
    public void SetUp(){
        searchNumber = mock(SearchNumber.class);
        searchNumber = new SearchNumber();
    }
    @Test
    public void test(){
        assertTrue(true);
    }

    @Test
    public void testFirst(){
        List list = mock(List.class);
        assertEquals(0, list.size());
        assertFalse(list.isEmpty());
        searchNumber.find("1", "40");
        assertEquals("Error", 3, 3);

    }



}

