package com;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SearchNumberTest {


    private SearchNumber searchNumber;

    @Before
    public void initTest(){
        searchNumber  = new SearchNumber();
    }



    @Test(expected = NumberFormatException.class)
    public void testParseIntNumbersWrong()throws MyException{
        searchNumber.parseIntNumbers("qqq");
    }

    @Test
    public void testParseIntNumbers() throws MyException{
        searchNumber.parseIntNumbers("10");
    }

    @Test
    public void testSearchNumber() throws MyException{
        searchNumber.find("1", "10");

    }
    @Test
    public void testSearchNumberWrong() throws MyException{
        searchNumber.find("40", "10");

    }
    @Test
    public void testSearchNumberWrongMinus() throws MyException{
        searchNumber.find("-10", "10");

    }
    @Ignore
    public void testSearchNumberWrongMinus111 (){

    }
    @Test
    public void testPrint() throws MyException{
        searchNumber.print();
    }
    @Ignore
    public void testSearchNumberPrint() throws MyException {
        searchNumber.print();
    }

    @Test
    public void testEqualsParseIntNumbers() throws MyException{
        int  n = searchNumber.parseIntNumbers("10");
        assertEquals(10, n);
    }
    @Test
    public void testAssertEquals(){
        searchNumber.find("1", "40");
        org.junit.Assert.assertEquals("Error", 3, 3);

    }



}
