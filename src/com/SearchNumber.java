package com;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Евгений on 31.07.2017.
 */
public class SearchNumber {
    Scanner scanner = new Scanner(System.in);
    private boolean simple = false;
    List<Integer> simpleNumbers = new ArrayList<>();

    public int parseIntNumbers(String string)throws MyException  {

        int number = 0;
        try {
            number = Integer.parseInt(string);
        } catch (java.lang.NumberFormatException e) {
            System.out.println("Error!! " + string);

        }

        return number;
    }



    public void find(String firstNumber, String lastNumber) throws MyException{
        int first = parseIntNumbers(firstNumber);
        int last = parseIntNumbers(lastNumber);
        if (first>= last){
            System.out.println("Error firstNumber > lastNumber");
            throw new MyException();
        }
        if (first<= 0|| last<= 0){
            System.out.println("Error firstNumber <= 0 or lastNumber <= 0");
            throw new MyException();
        }
        for (int i = first; i <= last; i++) {
            for (int j = 2; j < i; j++) {
                if (i % j != 0) {
                    simple = true;
                } else {
                    simple = false;
                    break;
                }
            }
            if (simple) {
                simpleNumbers.add(i);
            }
        }

    }

    public void print(){

        simpleNumbers.forEach(number-> System.out.println(number+ " - simple number"));
    }

}